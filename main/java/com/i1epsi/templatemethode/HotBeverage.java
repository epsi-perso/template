/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.templatemethode;

/**
 *
 * @author TVall
 */
public abstract class HotBeverage {
    final void prepare() {
        boilWater();
        addSugar();
    }

    void boilWater() { System.out.println("boid water");}
    
    void addSugar() { System.out.println("add sugar");}
    
}
