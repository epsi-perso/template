/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.templatemethode;

/**
 *
 * @author TVall
 */
public class HotBeverageTest {
    void prepare(Tea){
        var coffee = new Coffee();
        coffee.prepare();
        var tea = new Tea();
        tea.prepare();

        beverageList.add(coffee);
        beverageList.add(tea);
    }
    
}

